#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <math.h>
#include <SFML/Graphics.hpp>

#include "webcam-v4l2/webcam.h"

#define WIDTH  640
#define HEIGHT 480

#define MONITORWIDTH  1920
#define MONITORHEIGHT 1080
#define MONITORXOFFSET 1280
#define MONITORYOFFSET 1080

using std::vector;

void click_mouse(){
	system("xdotool click 1");
}

vector <sf::Vector2u> get_hand_positions(const sf::Image img){
	vector <unsigned> xPositions;
	vector <unsigned> yPositions;

	for (unsigned y=0; y<HEIGHT; y++){
		for (int x=0; x<WIDTH; x++){
			const sf::Color c = img.getPixel(x,y);
			if ((c.g-30 > c.r) && (c.g-40 > c.b)){
				xPositions.push_back(WIDTH-x);
				yPositions.push_back(y);
			}
		}
	}

	vector <sf::Vector2u> positions;

	for (unsigned i=0; i<xPositions.size(); i++)
		positions.push_back(sf::Vector2u(xPositions[i], yPositions[i]));

	return positions;
}

sf::Vector2u get_hand_position(const sf::Image img){
	vector <unsigned> xPositions;
	vector <unsigned> yPositions;

	for (unsigned y=0; y<HEIGHT; y++){
		for (unsigned x=0; x<WIDTH; x++){
			const sf::Color c = img.getPixel(x,y);
			if ((c.g-30 > c.r) && (c.g-40 > c.b)){
				xPositions.push_back(WIDTH-x);
				yPositions.push_back(y);
			}
		}
	}

	std::sort(xPositions.begin(), xPositions.end());
	std::sort(yPositions.begin(), yPositions.end());

	if (xPositions.empty() || yPositions.empty())
		return sf::Vector2u(0,0);

	if (xPositions.size() < 200)
		return sf::Vector2u(0,0);

	sf::Vector2u medianPosition = sf::Vector2u(xPositions[xPositions.size()/2], yPositions[yPositions.size()/2]);

	return medianPosition;
}

sf::Vector2u get_hand_position_red(const sf::Image img){
	vector <unsigned> xPositions;
	vector <unsigned> yPositions;

	for (unsigned y=0; y<HEIGHT; y++){
		for (unsigned x=0; x<WIDTH; x++){
			const sf::Color c = img.getPixel(x,y);
			if ((c.r-60 > c.g) && (c.r-55 > c.b) && (c.g < 60) && (c.b < 45)){
				xPositions.push_back(WIDTH-x);
				yPositions.push_back(y);
			}
		}
	}

	std::sort(xPositions.begin(), xPositions.end());
	std::sort(yPositions.begin(), yPositions.end());

	if (xPositions.empty() || yPositions.empty())
		return sf::Vector2u(0,0);

	if (xPositions.size() < 10)
		return sf::Vector2u(0,0);

	sf::Vector2u medianPosition = sf::Vector2u(xPositions[xPositions.size()/2], yPositions[yPositions.size()/2]);

	return medianPosition;
}

vector <sf::Vector2u> get_hand_positions_red(const sf::Image img){
	vector <unsigned> xPositions;
	vector <unsigned> yPositions;

	for (unsigned y=0; y<HEIGHT; y++){
		for (int x=0; x<WIDTH; x++){
			const sf::Color c = img.getPixel(x,y);
			if ((c.r-60 > c.g) && (c.r-55 > c.b) && (c.g < 60) && (c.b < 45)){
				xPositions.push_back(WIDTH-x);
				yPositions.push_back(y);
			}
		}
	}

	vector <sf::Vector2u> positions;

	for (unsigned i=0; i<xPositions.size(); i++)
		positions.push_back(sf::Vector2u(xPositions[i], yPositions[i]));

	return positions;
}

int main(){
	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "ironman", sf::Style::Titlebar | sf::Style::Close);
    Webcam webcam("/dev/video0", WIDTH, HEIGHT);

	sf::Texture pointTxt;
	pointTxt.loadFromFile("point.png");
	sf::Sprite pointSpr;
	pointSpr.setTexture(pointTxt);

	sf::Texture redPointTxt;
	redPointTxt.loadFromFile("redpoint.png");
	sf::Sprite redPointSpr;
	redPointSpr.setTexture(redPointTxt);

	sf::Texture medianTxt;
	medianTxt.loadFromFile("median.png");
	sf::Sprite medianSpr;
	medianSpr.setTexture(medianTxt);

	bool lastClicked = false;

	while (window.isOpen()){
		sf::Event e;
		while (window.pollEvent(e)){
			if (e.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		auto frame = webcam.frame();

		sf::Image frameImg;
		frameImg.create(WIDTH, HEIGHT, (sf::Uint8 *) frame.data);

		const sf::Vector2u handPos = get_hand_position(frameImg);

		if ((handPos.x != 0) && (handPos.y != 0)){
			sf::Vector2u deltaHandPos{handPos.x - WIDTH/2, handPos.y - HEIGHT/2};

			deltaHandPos.x /= 2;
			deltaHandPos.y /= 2;

//			deltaHandPos.x = deltaHandPos.x * (MONITORWIDTH / WIDTH);
//			deltaHandPos.y = deltaHandPos.y * (MONITORHEIGHT / HEIGHT);

			sf::Vector2i mousePos = sf::Mouse::getPosition();
			sf::Mouse::setPosition(sf::Vector2i(mousePos.x + deltaHandPos.x, mousePos.y + deltaHandPos.y));
		}

		const sf::Vector2u handRedPos = get_hand_position_red(frameImg);
		bool click = (handRedPos.x != 0) && (handRedPos.y != 0);

		if (click && !lastClicked)
			click_mouse();

		lastClicked = click;

		const auto handPositions = get_hand_positions(frameImg);
		const auto handPositionsRed = get_hand_positions_red(frameImg);

		// Draw webcam img
		sf::Texture txt;
		txt.loadFromImage(frameImg);
		sf::Sprite spr;
		spr.setTexture(txt);
		// Flip sprite
		spr.setTextureRect(sf::IntRect(WIDTH, 0, -WIDTH, HEIGHT));
		window.draw(spr);

		for (sf::Vector2u pos : handPositions){
			pointSpr.setPosition(pos.x, pos.y);
			window.draw(pointSpr);
		}

		for (sf::Vector2u pos : handPositionsRed){
			redPointSpr.setPosition(pos.x, pos.y);
			window.draw(redPointSpr);
		}

		medianSpr.setPosition(handPos.x, handPos.y);
		window.draw(medianSpr);
		medianSpr.setPosition(handRedPos.x, handRedPos.y);
		window.draw(medianSpr);

		window.display();
	}
}
